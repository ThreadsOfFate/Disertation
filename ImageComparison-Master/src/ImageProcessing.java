import java.io.File;
import java.io.FilenameFilter;

public class ImageProcessing {
    public static void main(String [] args){
        System.out.println("\n This is image comparison tool, please use the following syntax to work with it:\n" +
                "    <toolname> <first image> <second image> <colorTolerance threshold>");
        
        	
	        File file = new File("Images");
	        String[] directories = file.list(new FilenameFilter() {
	          @Override
	          public boolean accept(File current, String name) {
	            return new File(current, name).isDirectory();
	          }
	        });
	        File originalFile;
            File modifiedFile;
            int tolerance;
	        
	        
	        for(int i = 0; directories.length>i;i++)
	        {
	        	originalFile = new File("Normal/Test1.png");
	            modifiedFile = new File("Images/"+ directories[i] +"/Test1.png");
	            tolerance = Integer.parseInt("0");

	            ImageComparison imageComparison = new ImageComparison(originalFile, modifiedFile, tolerance);
	            imageComparison.compareImages(directories[i] + "Test1Result");
	            
	            originalFile = new File("Normal/Test2.png");
	            modifiedFile = new File("Images/"+ directories[i] +"/Test2.png");
	            tolerance = Integer.parseInt("0");

	            imageComparison = new ImageComparison(originalFile, modifiedFile, tolerance);
	            imageComparison.compareImages(directories[i] + "Test2Result");
	            
	            originalFile = new File("Normal/Test3.png");
	            modifiedFile = new File("Images/"+ directories[i] +"/Test3.png");
	            tolerance = Integer.parseInt("0");

	            imageComparison = new ImageComparison(originalFile, modifiedFile, tolerance);
	            imageComparison.compareImages(directories[i] + "Test3Result");
	        }
	        
        	
        
        
    }
}
